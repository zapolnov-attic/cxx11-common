/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __fbd85c2f71648869b8484dba4bf9354c__
#define __fbd85c2f71648869b8484dba4bf9354c__

#include <vector>

namespace Util
{
	/**
	 * A set of non-negative numbers.
	 * @author Nikolay Zapolnov
	 */
	class IntSet
	{
	public:
		/**
		 * Constructor.
		 * @param size Initial size of the set.
		 */
		inline IntSet(size_t size = 0) : m_Bitmap(size) {}

		/**
		 * Copy constructor.
		 * @param src Source set.
		 */
		inline IntSet(const IntSet & src) : m_Bitmap(src.m_Bitmap) {}

		/**
		 * Move constructor.
		 * @param src Source set.
		 */
		inline IntSet(IntSet && src) : m_Bitmap(std::move(src.m_Bitmap)) {}

		/** Destructor. */
		inline ~IntSet() {}

		/**
		 * Assignment operator with copy semantics.
		 * @param src Source set.
		 * @return Reference to this set.
		 */
		inline IntSet & operator=(const IntSet & src) { m_Bitmap = src.m_Bitmap; return *this; }

		/**
		 * Assignment operator with move semantics.
		 * @param src Source set.
		 * @return Reference to this set.
		 */
		inline IntSet & operator=(IntSet && src) { m_Bitmap = std::move(src.m_Bitmap); return *this; }

		/** Clears the set. */
		inline void clear() { reset(m_Bitmap.size()); }

		/**
		 * Clears and reallocates the set.
		 * @param size New size of the set.
		 */
		void reset(size_t size);

		/**
		 * Checks whether this set contains the specified number.
		 * @param n Number to check.
		 * @return *true* if the specified number is in the set, *false* otherwise.
		 */
		bool contains(int n) const;

		/**
		 * Adds number into the set.
		 * @param n Number to add.
		 */
		void add(int n);

		/**
		 * Adds all numbers from the given set into this set.
		 * This is an equivalent of the 'union' operation.
		 * @param oth Set to add numbers from.
		 * @return *true* if operation has changed this set, *false* otherwise.
		 */
		bool add(const IntSet & oth);

		/**
		 * Removes number from the set.
		 * @param n Number to remove.
		 */
		void remove(int n);

		/**
		 * Removes all numbers in the given set from this set.
		 * @param oth Set of numbers to remove.
		 * @return *true* if operation has changed this set, *false* otherwise.
		 */
		bool remove(const IntSet & oth);

	private:
		std::vector<bool> m_Bitmap;
	};
}

#endif
