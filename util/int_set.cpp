/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "int_set.h"
#include <stdexcept>

using Util::IntSet;

void IntSet::reset(size_t size)
{
	m_Bitmap.clear();
	m_Bitmap.resize(size);
}

bool IntSet::contains(int n) const
{
	if (n < 0)
		return false;

	size_t v = (size_t)n;
	return v < m_Bitmap.size() && m_Bitmap[v];
}

void IntSet::add(int n)
{
	if (n < 0)
		throw std::range_error("number is out of range.");

	size_t v = (size_t)n;
	if (v > m_Bitmap.size())
		m_Bitmap.resize(v + 1);
	m_Bitmap[v] = true;
}

bool IntSet::add(const IntSet & oth)
{
	bool changed = false;

	if (oth.m_Bitmap.size() > m_Bitmap.size())
		m_Bitmap.resize(oth.m_Bitmap.size());

	for (size_t i = 0; i < std::min(m_Bitmap.size(), oth.m_Bitmap.size()); i++)
	{
		bool before = m_Bitmap[i];
		bool after = before || oth.m_Bitmap[i];
		if (after != before)
		{
			changed = true;
			m_Bitmap[i] = after;
		}
	}

	return changed;
}

void IntSet::remove(int n)
{
	if (n < 0)
		return;

	size_t v = (size_t)n;
	if (v < m_Bitmap.size())
		m_Bitmap[v] = false;
}

bool IntSet::remove(const IntSet & oth)
{
	bool changed = false;

	for (size_t i = 0; i < std::min(m_Bitmap.size(), oth.m_Bitmap.size()); i++)
	{
		bool before = m_Bitmap[i];
		bool after = before && !oth.m_Bitmap[i];
		if (after != before)
		{
			changed = true;
			m_Bitmap[i] = after;
		}
	}

	return changed;
}
